<?php

declare(strict_types=1);

namespace Adapter;

class BookAdapter
{
    public function __construct(private readonly SimpleBook $book)
    {
    }

    public function getAuthorAndTitle(): string
    {
        return $this->book->getTitle().' by '.$this->book->getAuthor();
    }
}
