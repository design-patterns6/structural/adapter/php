<?php

declare(strict_types=1);

namespace Adapter;

class SimpleBook
{
    public function __construct(
        private readonly string $author,
        private readonly string $title
    ) {
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
