<?php

declare(strict_types=1);

namespace Adapter\Tests;

use Adapter\BookAdapter;
use Adapter\SimpleBook;
use PHPUnit\Framework\TestCase;

class AdapterTest extends TestCase
{
    public function testBookAdapterShouldReturnConcatenateAuthorAndTitle(): void
    {
        // GIVEN
        $book = new SimpleBook('Gamma, Helm, Johnson, and Vlissides', 'Design Patterns');
        $bookAdapter = new BookAdapter($book);
        // THEN
        self::assertEquals(expected: 'Design Patterns by Gamma, Helm, Johnson, and Vlissides', actual: $bookAdapter->getAuthorAndTitle());
    }
}
